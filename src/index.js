import React, { createContext, useContext, useReducer, Component } from 'react';
import {Store, CombineStore} from '../types'

/**
 * A class that implements and simplifies React Context
 */
export class Store {
  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {object} initValue - Value to initialize the store with
   */
  constructor(displayName = 'React Store', initValue = {}) {
    this.context = createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to regularly import useContext to access context value
   *
   * @returns Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore() {
    return useContext(this.context);
  }

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: any, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider({ value, children }) {
    const StoreContext = this.context;
    return (
      <StoreContext.Provider value={value}>{children}</StoreContext.Provider>
    );
  }
}

/**
 * A class that implements and simplifies React Context with Combine Reducer
 */
export class CombineStore {
  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   * @param {any} initValue - Value to initialize the store with
   */
  constructor(displayName = 'Combine Context', initValue = {}) {
    this.context = createContext(initValue);
    this.context.displayName = displayName;
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to regularly import useContext to access context value
   *
   * @returns Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = () => useContext(this.context);

  /**
   * A function that creates provider state for context provider value
   *
   * @param {{[string]:[any, function, object:{[string]: object | function }]}} combineState state object to be combined
   * @returns {CombinedStore}
   */
  createProviderState(combineState) {
    // Get keys of the combine reducer
    const keys = Object.keys(combineState);
    // switch reducer function for dispatch function using useReducer
    const combinedState = keys.reduce((acc, key) => {
      const [initState, stateReducer, stateAction] = combineState[key];
      const [state, dispatch] = useReducer(stateReducer, initState);
      return {
        ...acc,
        [key]: [state, dispatch, stateAction],
      };
    }, {});
    return combinedState;
  }

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: CombineStore, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider = ({ value, children }) => {
    const state = this.createProviderState(value);
    const StoreContext = this.context;
    return (
      <StoreContext.Provider value={state}>{children}</StoreContext.Provider>
    );
  };
}

const AllStores = { Store, CombineStore };

export default AllStores;
