import React from 'react'

/**
 * State object
 */
interface State {
  [key: string]: any
}

/**
 * Action object
 */
interface Action {
  type: string
  [data: string]: any
}

/**
 * Action objects / functions
 */
// interface Actions<A> {
//   [action: string]: A | ((...args: any[]) => A)
// }

/**
 * Array of [State object, Reducer function, Actions object]
 */
type InitStoreArrType<S, A> = [S, React.Reducer<S, A>]

/**
 * Object with combine store data
 */
interface CombineStoreType<S, A> {
  [name: string]: InitStoreArrType<S, A>
}

/**
 * Array of [State object, Dispatch function, Actions object]
 */
type ResultStoreArrType<S, A> = [S, React.Dispatch<A>]

/**
 * Object with combined store data
 */
interface CombinedStoreType<S, A> {
  [name: string]: ResultStoreArrType<S, A>
}

/**
 * A class that implements and simplifies React Context
 */
export class CustomStore<S extends State> {
  // Initialize the context
  context: React.Context<S> = React.createContext({} as S)

  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   */
  constructor(displayName: string = 'React Context Store') {
    this.context.displayName = displayName
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {S} Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): S => React.useContext(this.context)

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: any, children: React.ReactChildren}} param0 Nested children of component
   * @returns
   */
  Provider = ({ value, children }: { value: S; children: React.ReactNode }) => {
    const StoreContext = this.context
    return React.createElement(StoreContext.Provider, { value }, children)
  }
}

/**
 * A class that implements React Context with a redux-like interface
 */
export class CustomReduxStore<S extends State, A extends Action> {
  // Initialize the context
  context: React.Context<ResultStoreArrType<S, A>> = React.createContext(
    [] as unknown as ResultStoreArrType<S, A>
  )

  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   */
  constructor(displayName: string = 'React Redux Store') {
    this.context.displayName = displayName
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {ResultStoreArrType<S, A>} Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): [S, React.Dispatch<A>] => React.useContext(this.context)

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: InitStoreArrType<S, A>, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider = ({
    value: [initState, stateReducer],
    children
  }: {
    value: InitStoreArrType<S, A>
    children: React.ReactNode
  }) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [state, dispatch] = React.useReducer(stateReducer, initState)
    const StoreContext = this.context
    return React.createElement(
      StoreContext.Provider,
      { value: [state, dispatch] },
      children
    )
  }
}

/**
 * A class that implements React Context with a redux-like interface having the combine store functionality
 */
export class CustomCombineStore<S extends State, A extends Action> {
  // Initialize the context
  context: React.Context<CombinedStoreType<S, A>> = React.createContext(
    {} as CombinedStoreType<S, A>
  )

  /**
   *
   * @param {string} displayName - Display name of the store for debugging
   */
  constructor(displayName: string = 'Combine Redux Store') {
    this.context.displayName = displayName
  }

  /**
   * A function that returns the store.
   *
   * Note: The function prevents the need to import useContext to access context value
   *
   * @returns {CombinedStoreType<S, A> } Current store state
   */
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useStore = (): CombinedStoreType<S, A> => React.useContext(this.context)

  /**
   * A function that creates provider state for context provider value
   *
   * @param {CombinedStoreType<S, A>} combineState state object to be combined
   * @returns {CombinedStoreType<S, A>}
   */
  createProviderState = (
    combineState: CombineStoreType<S, A>
  ): CombinedStoreType<S, A> => {
    // Get keys of the combine reducer
    const keys = Object.keys(combineState)
    // switch reducer function for dispatch function using useReducer
    const combinedState = keys.reduce((acc, key) => {
      const [initState, stateReducer] = combineState[key]
      // eslint-disable-next-line react-hooks/rules-of-hooks
      const [state, dispatch] = React.useReducer(stateReducer, initState)
      return {
        ...acc,
        [key]: [state, dispatch]
      }
    }, {})
    return combinedState
  }

  /**
   * A component that provides a global store to it's children component.
   *
   * @param {{value: CombineStoreType<S, A>, children: React.ReactNode}} param0 Nested children of component
   * @returns
   */
  Provider = ({
    value,
    children
  }: {
    value: CombineStoreType<S, A>
    children: React.ReactNode
  }) => {
    const state = this.createProviderState(value)
    const StoreContext = this.context
    return React.createElement(
      StoreContext.Provider,
      { value: state },
      children
    )
  }
}

const AllStores = { CustomStore, CustomReduxStore, CustomCombineStore }

export default AllStores
