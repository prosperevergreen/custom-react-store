import React from 'react';

export type Action = { type: string; [data: string]: any };
export type Actions = {
  [action: string]: Action | ((...args: any[]) => Action);
};
export type CombineStoreArrType<S, A> = [S, React.Reducer<S, A>, A];
export type CombineStoreType<S, A> = {
  [name: string]: CombineStoreArrType<S, A>;
};

export type StoreProviderComponent<S,A> = {
  value: CombineStoreType<S,A>;
  children: React.ReactNode;
};

/**
 * A component that provides a global store to it's children component.
 *
 * @param {{value: CombineStore; children: React.ReactNode}} props Nested children of component
 * @returns {React.FunctionComponentElement<React.ProviderProps<{}>>}
 */
export type StoreProviderType<S,A> = (
  props: StoreProviderComponent<S,A>
) => React.FunctionComponentElement<
  React.ProviderProps<StoreProviderComponent<S,A>>
>;

export type CombinedStoreArrType<S, A> = [S, React.Dispatch<Action>, A];
export type CombinedStoreType<S, A> = {
  [name: string]: CombinedStoreArr<S, A>;
};
/**
 * A function that returns the store.
 *
 * Note: The function prevents the need to regularly import useContext to access context value
 *
 * @returns Current store state
 */
export type UseStore<S,A> = () => CombinedStore<S,A>;

export interface Store<S> {
  context: React.Context<S>;
  constructor(displayName: string, initValue: S | {}): void;
  useStore(): S;
  Provider(S: S, children: React.ReactChildren): React.Provider<S>;
}

export interface CombineStore<S,A> {
  context: React.Context<S,A>;
  constructor(displayName: string, initValue: CombineStoreType<S,A> | {}): void;
  useStore: UseStore<S,A>;
  Provider: StoreProviderType<S,A>;
}
