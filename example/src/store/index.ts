import * as counter from "./counterSlice";

type CombineStoreSetup = {
    counter3Store: [typeof counter.initialState, typeof counter.reducer],
    counter4Store: [typeof counter.initialState, typeof counter.reducer],
}

/**
 * Object that combines different store slices
 */
const combineStoreSetup : CombineStoreSetup = {
    // The format for combining reducer is as follows
    // sliceName: [sliceInitialState, sliceReducer, sliceActions ]
	counter3Store: [counter.initialState, counter.reducer],
	counter4Store: [counter.initialState, counter.reducer],
};

export default combineStoreSetup;
