export const initialState = {
  counter: 0
}

export type ActionType =
  | {
      type: 'CHANGE'
      payload: number
    }
  | { type: 'RESET' }

export const reducer = (state: typeof initialState, action: ActionType) => {
  switch (action.type) {
    case 'CHANGE':
      return { ...state, counter: state.counter + action.payload }
    case 'RESET':
      return { ...state, counter: 0 }
    default:
      return state
  }
}

export const actions: {
  change(payload: number): {
    type: 'CHANGE'
    payload: number
  }
  reset: { type: 'RESET' }
} = {
  change: (payload: number) => ({ type: 'CHANGE', payload }),
  reset: { type: 'RESET' }
}
