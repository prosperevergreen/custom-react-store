import React, { useState } from 'react'
import {
  CustomStore,
  CustomReduxStore,
  CustomCombineStore
} from 'custom-react-store'
import * as CounterSetup from './store/counterSlice'
import combineCounterSetup from './store'
import Counter1 from './components/Counter1/Counter1'
import Counter2 from './components/Counter2/Counter2'
import Counter3 from './components/Counter3/Counter3'
import Counter4 from './components/Counter4/Counter4'

export const customStore = new CustomStore<{
  counter1: number
  setCounter1: React.Dispatch<React.SetStateAction<number>>
}>('Custom Store')

export const customReduxStore = new CustomReduxStore<
  typeof CounterSetup.initialState,
  CounterSetup.ActionType
>('Custom Redux Store')

export const customCombineStore = new CustomCombineStore<
  typeof CounterSetup.initialState,
  CounterSetup.ActionType
>('Custom Combine Store')

const App = () => {
  const [counter1, setCounter1] = useState(0)
  return (
    <div>
      <h1>custome-react-store Examples</h1>
      <ul>
        <li>
          <customStore.Provider value={{ counter1, setCounter1 }}>
            <h3>Custom Store Example</h3>
            <Counter1 />
          </customStore.Provider>
        </li>
        <li>
          <customReduxStore.Provider
            value={[CounterSetup.initialState, CounterSetup.reducer]}
          >
            <h3>Custom Redux Store Example</h3>
            <Counter2 />
          </customReduxStore.Provider>
        </li>
        <customCombineStore.Provider value={combineCounterSetup}>
          <li>
            <h3>Custom Combine Store Example</h3>
            <Counter3 />
          </li>
          <li>
            <h3>Custom Combine Store Example</h3>
            <Counter4 />
          </li>
        </customCombineStore.Provider>
      </ul>
    </div>
  )
}

export default App
