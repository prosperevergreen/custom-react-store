import React from 'react'
import { customStore } from '../../App'

const Counter1 = () => {
  const { counter1, setCounter1 } = customStore.useStore()
  return (
    <div>
      <div>{counter1}</div>
      <div>
        <button onClick={() => setCounter1(counter1 - 1)}>-</button>
        <button onClick={() => setCounter1(0)}>reset</button>
        <button onClick={() => setCounter1(counter1 + 1)}>+</button>
      </div>
    </div>
  )
}

export default Counter1
