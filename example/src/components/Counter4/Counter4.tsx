import React from 'react'
import { customCombineStore } from '../../App'
import { actions } from '../../store/counterSlice'

const Counter4 = () => {
  const { counter4Store } = customCombineStore.useStore()
  const [{ counter: counter4 }, counter4Dispatch] = counter4Store
  return (
    <div>
      <div>{counter4}</div>
      <div>
        <button onClick={() => counter4Dispatch(actions.change(-1))}>-</button>
        <button onClick={() => counter4Dispatch(actions.reset)}>reset</button>
        <button onClick={() => counter4Dispatch(actions.change(+1))}>+</button>
      </div>
    </div>
  )
}

export default Counter4
