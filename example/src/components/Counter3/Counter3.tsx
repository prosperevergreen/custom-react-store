import React from 'react'
import { customCombineStore } from '../../App'
import { actions } from '../../store/counterSlice'

const Counter3 = () => {
  const {counter3Store} = customCombineStore.useStore()
  const [{ counter: counter3 }, counter3Dispatch] = counter3Store
  return (
    <div>
      <div>{counter3}</div>
      <div>
        <button onClick={() => counter3Dispatch(actions.change(-1))}>-</button>
        <button onClick={() => counter3Dispatch(actions.reset)}>reset</button>
        <button onClick={() => counter3Dispatch(actions.change(+1))}>+</button>
      </div>
    </div>
  )
}

export default Counter3
