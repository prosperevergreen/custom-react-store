import React from 'react'
import { customReduxStore } from '../../App'
import { actions } from '../../store/counterSlice'

const Counter2 = () => {
  const [{ counter: counter2 }, counter2Dispatch] = customReduxStore.useStore()
  return (
    <div>
      <div>{counter2}</div>
      <div>
        <button onClick={() => counter2Dispatch(actions.change(-1))}>-</button>
        <button onClick={() => counter2Dispatch(actions.reset)}>reset</button>
        <button onClick={() => counter2Dispatch(actions.change(+1))}>+</button>
      </div>
    </div>
  )
}

export default Counter2
